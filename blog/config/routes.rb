Rails.application.routes.draw do
  get '/signup' => 'users#new'
  resources :users
  resources :articles
  get '/login' => 'session#new', as: :login
  post 'login' => 'session#create'
  delete '/logout' => 'session#destroy'
  get '/your_posts' => 'articles#user_articles', as: :your_posts

  root 'session#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

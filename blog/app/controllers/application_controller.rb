class ApplicationController < ActionController::Base
  helper_method :current_user

  add_flash_types :danger, :success, :warning
	def current_user
	  @current_user ||= User.find_by(id: session[:user_id]) if session[:user_id]
	end

	def require_user
  	unless current_user
  		redirect_to login_path, notice: 'Faça login'
  	end
	end
end

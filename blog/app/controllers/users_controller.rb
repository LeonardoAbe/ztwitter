class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]


  # GET /users
  # GET /users.json
  def index
    if !current_user
      @users = User.all
    else
      redirect_to articles_path
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show

  end

  def edit

  end

  # GET /users/new
  def new
    if !current_user
      @user = User.new
      render layout: "signup"
    else
      redirect_to articles_path
    end
  end


  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to login_path, notice: 'Perfil cadastrado com sucesso'
    else
      render :new, notice: 'Não foi possível cadastra-lo, preencha todos os campos'
    end

  end

  def update
    if @user.update(user_params)
      redirect_to @user, notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    redirect_to users_url, notice: 'User was successfully destroyed.'
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :password)
    end
end

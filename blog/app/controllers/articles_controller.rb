class ArticlesController < ApplicationController
  before_action :require_user, only: [:index, :show, :create, :edit, :update, :destroy, :new, :user_articles]
  before_action :set_article, only: [:show, :edit, :update, :destroy]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
    @article = Article.new
  end

  def user_articles
    @articles = Article.where(user_id: session[:user_id])
    @article = Article.new
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @article = Article.find(params[:id])
  end

  # GET /articles/new
  #def new
    #@article = Article.new
  #end

  # POST /articles
  # POST /articles.json
  def create
    @article = current_user.articles.new(article_params)
    if @article.save
      redirect_back fallback_location: request.referrer, success: "Post criado!"
    else
      redirect_back fallback_location: request.referrer, danger: "O post deve conter algo!"
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    if @current_user.id == @article.user_id
      @article.destroy
      redirect_to articles_path, warning: 'Post deletado.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:body).merge(:user_name => @current_user.name)
    end
end
